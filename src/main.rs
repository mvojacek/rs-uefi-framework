#![no_std]
#![no_main]

#![feature(alloc_error_handler)]
#![feature(abi_efiapi)]

#![feature(optin_builtin_traits)]
#![feature(lang_items)]
#![feature(core_intrinsics)]

extern crate alloc;

#[doc(hidden)]
pub use uefi::Identify;
#[doc(hidden)]
pub use uefi::Guid;

use uefi::prelude::*;

mod globals;

#[panic_handler]
fn panic_handler(info: &core::panic::PanicInfo) -> ! {
    let st = unsafe { globals::system_table().as_ref() };
    let _ = st.stdout().write_fmt(format_args!("{}\n", info));
    loop {
        st.boot_services().stall(1_000_000_000);
    }
}

#[lang = "eh_personality"]
extern fn eh_personality() {}

#[alloc_error_handler]
fn alloc_err_handler(_layout: core::alloc::Layout) -> ! {
    loop {}
}

use core::fmt::Write;
use core::mem;
use core::cmp::min;
use uefi::proto::media::fs::SimpleFileSystem;
use alloc::vec::Vec;
use uefi::CStr16;
use alloc::string::String;

mod proto;

use proto::net::simple::SimpleNetworkProtocol;

#[entry]
fn efi_main(_handle: Handle, st: SystemTable<Boot>) -> Status {
    let bs = st.boot_services();

    unsafe {
        globals::set_system_table(&st);
        uefi::alloc::init(bs);
    }

    st.stdout().write_str("STUFF start\n").unwrap();

    let snp = unsafe { bs.locate_protocol::<SimpleNetworkProtocol>().expect_success("locating net").get().as_ref() }.unwrap();

    snp.

    Status::ABORTED
}