use uefi::prelude::*;
use core::ptr::NonNull;

static mut SYSTEM_TABLE: Option<NonNull<SystemTable<Boot>>> = None;

pub unsafe fn set_system_table(system_table: &SystemTable<Boot>) {
    SYSTEM_TABLE = NonNull::new(system_table as *const _ as *mut _);
}

pub fn system_table() -> NonNull<SystemTable<Boot>> {
    unsafe { SYSTEM_TABLE.expect("System table is unavailable or have been exited") }
}