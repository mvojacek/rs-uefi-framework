use uefi::prelude::*;

use uefi::proto::Protocol;
use uefi::unsafe_guid;
use core::ptr;
use r_efi::{eficall, eficall_abi};

use bitflags::bitflags;

#[repr(C)]
#[unsafe_guid("a19832b9-ac25-11d3-9a2d-0090273fc14d")]
#[derive(Protocol)]
pub struct SimpleNetworkProtocol {
    revision: u64,
    start: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    stop: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    initialize: eficall! {fn(&mut SimpleNetworkProtocol, usize, usize) -> Status},
    reset: eficall! {fn(&mut SimpleNetworkProtocol, bool) -> Status},
    shutdown: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    receive_filters: eficall! {fn(&mut SimpleNetworkProtocol, SimpleNetworkReceive, SimpleNetworkReceive, bool, usize, *const EfiMacAddress) -> Status},
    station_address: eficall! {fn(&mut SimpleNetworkProtocol, bool, *const EfiMacAddress) -> Status},
    statistics: eficall! {fn(&mut SimpleNetworkProtocol, bool, *mut usize, *mut EfiNetworkStatistics) -> Status},
    mcast_ip_to_mac: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    //TODO
    nv_data: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    //TODO
    get_status: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    transmit: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    receive: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    wait_for_packet: eficall! {fn(&mut SimpleNetworkProtocol) -> Status},
    mode: *mut SimpleNetworkMode,
}

impl SimpleNetworkProtocol {
    /// Changes the state of a network interface from “stopped” to “started.”
    ///
    /// This function starts a network interface. If the network interface successfully starts, then
    /// EFI_SUCCESS will be returned.
    pub fn start(&mut self) -> Status {
        (self.start)(self)
    }

    /// Changes the state of a network interface from “started” to “stopped.”
    ///
    /// This function stops a network interface. This call is only valid if the network interface is in
    /// the started state. If the network interface was successfully stopped, then EFI_SUCCESS will
    /// be returned.
    pub fn stop(&mut self) -> Status {
        (self.stop)(self)
    }

    /// Resets a network adapter and allocates the transmit and receive buffers required by the network
    /// interface; optionally, also requests allocation of additional transmit and receive buffers.
    ///
    /// This function allocates the transmit and receive buffers required by the network interface. If this
    /// allocation fails, then EFI_OUT_OF_RESOURCES is returned. If the allocation succeeds and the
    /// network interface is successfully initialized, then EFI_SUCCESS will be returned.
    pub fn initialize(&mut self, extra_rx_buffer_size: Option<usize>, extra_tx_buffer_size: Option<usize>) -> Status {
        (self.initialize)(self, extra_rx_buffer_size.unwrap_or(0), extra_tx_buffer_size.unwrap_or(0))
    }

    /// Resets a network adapter and reinitializes it with the parameters that were provided in the
    /// previous call to Initialize().
    ///
    /// This function resets a network adapter and reinitializes it with the parameters that were provided
    /// in the previous call to Initialize(). The transmit and receive queues are emptied and all pending
    /// interrupts are cleared. Receive filters, the station address, the statistics, and the multicast-IP-to-
    /// HW MAC addresses are not reset by this call. If the network interface was successfully reset, then
    /// EFI_SUCCESS will be returned. If the driver has not been initialized, EFI_DEVICE_ERROR will be
    /// returned.
    pub fn reset(&mut self, extended_verification: bool) -> Status {
        (self.reset)(self, extended_verification)
    }

    pub fn shutdown(&mut self) -> Status {
        (self.shutdown)(self)
    }

    pub fn receive_filters(&mut self, enable: SimpleNetworkReceive, disable: SimpleNetworkReceive, reset_mcast_filter: bool, mcast_filter: &[EfiMacAddress]) -> Status {
        (self.receive_filters)(self, enable, disable, reset_mcast_filter, mcast_filter.len(), mcast_filter.as_ptr())
    }

    pub fn station_address(&mut self, reset: bool, new: Option<&EfiMacAddress>) -> Status {
        (self.station_address)(self, reset, new.map_or(ptr::null(), |addr| addr as *const _))
    }

    pub unsafe fn statistics_raw(&mut self, reset: bool, stats_size: &mut usize, stats: &mut EfiNetworkStatistics) -> Status {
        (self.statistics)(self, reset, stats_size, stats)
    }

    pub fn statistics(&mut self, reset: bool, stats: &mut EfiNetworkStatistics) -> Status {
        #[allow(unused_unsafe)]
            let expected_size = unsafe { core::intrinsics::size_of_val(stats) };
        let mut size = expected_size;
        let status = unsafe { self.statistics_raw(reset, &mut size, stats) };
        if status.is_success() {
            assert_eq!(size, expected_size);
        }
        status
    }
}

#[repr(C)]
pub struct EfiNetworkStatistics {
    rx_total_frames: u64,
    rx_good_frames: u64,
    rx_undersize_frames: u64,
    rx_oversize_frames: u64,
    rx_dropped_frames: u64,
    rx_unicast_frames: u64,
    rx_broadcast_frames: u64,
    rx_multicast_frames: u64,
    rx_crc_error_frames: u64,
    rx_total_bytes: u64,
    tx_total_frames: u64,
    tx_good_frames: u64,
    tx_undersize_frames: u64,
    tx_oversize_frames: u64,
    tx_dropped_frames: u64,
    tx_unicast_frames: u64,
    tx_broadcast_frames: u64,
    tx_multicast_frames: u64,
    tx_crc_error_frames: u64,
    tx_total_bytes: u64,
    collisions: u64,
}

#[repr(C)]
pub struct SimpleNetworkMode {
    state: u32,
    hw_address_size: u32,
    media_header_size: u32,
    max_packet_size: u32,
    nv_ram_size: u32,
    nv_ram_access_size: u32,
    receive_filter_mask: u32,
    receive_filter_setting: SimpleNetworkReceive,
    max_mcast_filter_count: u32,
    mcast_filter_count: u32,
    mcast_filter: [EfiMacAddress; MAX_MCAST_FILTER_CNT],
    current_address: EfiMacAddress,
    broadcast_address: EfiMacAddress,
    permanent_address: EfiMacAddress,
    if_type: u8,
    mac_address_changeable: bool,
    multiple_tx_supported: bool,
    media_present_supported: bool,
    media_present: bool,
}

const MAX_MCAST_FILTER_CNT: usize = 16;

#[repr(C)]
pub struct EfiMacAddress {
    addr: [u8; 32],
}

bitflags! {
    pub struct SimpleNetworkReceive: u32 {
        const UNICAST = 0x1;
        const MULTICAST = 0x2;
        const BROADCAST = 0x4;
        const PROMISCUOUS = 0x8;
        const PROMISCUOUS_MULTICAST= 0x10;
    }
}